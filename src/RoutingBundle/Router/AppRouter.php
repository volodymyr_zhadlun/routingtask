<?php

namespace RoutingBundle\Router;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;

class AppRouter extends Router
{
    /**
     * {@inheritdoc}
     */
    public function matchRequest(Request $request)
    {
        $matcher = $this->getMatcher();

        if ($route = $this->findControllerAction($request)) {
            return $route;
        }

        if (!$matcher instanceof RequestMatcherInterface) {
            return $matcher->match($request->getPathInfo());
        }

        return $matcher->matchRequest($request);
    }

    protected function findControllerAction(Request $request): ?array
    {
        $path = explode('/', strtolower($request->getPathInfo()));
        if (count($path) == 4) {
            $bundle = ucfirst($path[1]) . 'Bundle';
            $controller = ucfirst($path[2]) . 'Controller';
            $className = $bundle . '\\Controller\\' . $controller;
            $action = $path[3] . 'Action';

            if (class_exists($className)) {
                if (method_exists($className, $action)) {
                    return [
                        '_controller' => $className . '::' . $action,
                    ];
                }
            }
        }

        return null;
    }
}
