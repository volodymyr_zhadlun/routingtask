<?php
namespace AdminBundle\EventListener;

use AdminBundle\Controller\SecuredControllerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AccessListener
{
    public function onKernelController(FilterControllerEvent $event)
    {
        if($event->getController()[0] instanceof SecuredControllerInterface){
            if($event->getRequest()->headers->get('admin') != 'test'){
                throw new AccessDeniedHttpException('Access denied');
            }
        }
    }
}